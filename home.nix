{ pkgs, pkgs2311, ... }:

{
  # Home Manager needs a bit of information about you and the paths it should
  # manage.
  home.username = "tether";
  home.homeDirectory = "/home/tether";

  # This value determines the Home Manager release that your configuration is
  # compatible with. This helps avoid breakage when a new Home Manager release
  # introduces backwards incompatible changes.
  #
  # You should not change this value, even if you update Home Manager. If you do
  # want to update the value, then make sure to first check the Home Manager
  # release notes.
  home.stateVersion = "23.11"; # Please read the comment before changing.

  nixpkgs.config.allowUnfree = true;
  # The home.packages option allows you to install Nix packages into your
  # environment.

  # let patched = pkgs.callPackage ./patch-font.nix {font = pkgs.victor-mono;}; in
  home.packages = with pkgs; [
    ## broken
    pkgs2311.obsidian
    # thefuck
    ## misc tools
    # vivaldi
    wget
    free42
    pika-backup
    helvum
    obs-studio
    picard
    zip
    unzip
    ## fonts
    (nerdfonts.override { fonts = [ "FiraCode" "VictorMono" ]; })
    atkinson-hyperlegible
    fira-code
    ## games
    protonup-qt
    gamemode
    (lutris.override {
      extraLibraries = pkgs: [
        # List library dependencies here
      ];
      extraPkgs = pkgs: [
        # List package dependencies here
      ];
    })
    ## creation
    audacity
    kdenlive
    krita
    gimp
    prusa-slicer
    blender
    imagemagick
    pandoc
    libreoffice-fresh
    ### Development
    vscodium
    zed-editor
    godot_4
    openscad-unstable
    nixd
    nixpkgs-fmt
    rustup
    bacon
    zoxide
    starship
    eza
    fzf
    sl
    direnv
    zola
    just
    gcc
    wl-clipboard
    btop
    ## comms
    element-desktop
    halloy
    ## media players
    kdePackages.elisa
    vlc
    pandoc
    exiftool
  ];
  fonts.fontconfig.enable = true;

  # programs.plasma = { };

  # Home Manager is pretty good at managing dotfiles. The primary way to manage
  # plain files is through 'home.file'.
  home.file = {
    ## examples
    # ".screenrc".source = dotfiles/screenrc;
    # ".gradle/gradle.properties".text = ''
    #   org.gradle.console=verbose
    #   org.gradle.daemon.idletimeout=3600000
    # '';
    ".gitconfig".source = ./git.config;
    ".config/fish/" = {
      source = ./fish;
      recursive = true;
    };
    ".config/VSCodium/User/settings.json".source = ./codium.jsonc;
    ".config/vscode/User/settings.json".source = ./codium.jsonc;
    ".config/starship.toml".source = ./starship.toml;
    ".local/share/applications/" = {
      source = ./applications;
      recursive = true;
    };
  };


  # home.activation.davincibox =
  #   let db = "${pkgs.distrobox}/bin/distrobox";
  #   in lib.hm.dag.entryAfter [ "writeBoundry" ] ''
  #     export PATH=$PATH:${pkgs.podman}/bin
  #     run yes | \
  #       ${db} create -i ghcr.io/zelikos/davincibox:latest -n davincibox
  #   '';

  home.sessionVariables = {
    EDITOR = "vim";
    #TODO copy from config.fish
  };

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}

