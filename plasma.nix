{pkgs, ...}: {
  programs.plasma = {
    enable = true;
    workspace = {
      clickItemTo = "select";
      lookAndFeel = "org.kde.breezedark.desktop";
      cursorTheme = "Breeze Light";
      iconTheme = "Breeze Dark";
      wallpaper = "${pkgs.libsForQt5.breeze-qt5}/share/wallpapers/Next/contents/images_dark/1920x1080.png";
    };
    fonts = rec {
      general = {
        family = "Atkinson Hyperlegible";
        pointSize = 11;
      };
      toolbar = general;
      menu = general;
      windowTitle = {
        family = general.family;
        pointSize = 9;
      };
      fixedWidth = {
        family = "Victor Mono";
        pointSize = 10;
      };
    };
    # spectacle.shortcuts = {
    #   captureRectangularRegion = "Meta+Shift+S";
    # };
  };
}