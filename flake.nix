{
  description = "Home Manager configuration of tether";

  inputs = {
    # Specify the source of Home Manager and Nixpkgs.
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    nixpkgs2311.url = "github:nixos/nixpkgs/nixos-23.11";

    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    plasma-manager = {
      url = "github:pjones/plasma-manager";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.home-manager.follows = "home-manager";
    };
    # nix-citizen.url = "github:LovingMelody/nix-citizen";
    # nix-gaming.url = "github:fufexan/nix-gaming";
  };

  outputs = inputs @ { nixpkgs, nixpkgs2311, home-manager, plasma-manager, ... }:
    let
      system = "x86_64-linux";
      pkgs = nixpkgs.legacyPackages.${system};
      pkgs2311 = import nixpkgs2311 {
        inherit system;
        config.allowUnfree = true;
        config.permittedInsecurePackages = [ "electron-25.9.0" ];
      };
    in
    {
      homeConfigurations."tether" = home-manager.lib.homeManagerConfiguration {
        inherit pkgs;
        # Specify your home configuration modules here, for example,
        # the path to your home.nix.
        extraSpecialArgs = {
          pkgs2311 = pkgs2311;
        };
        modules = [
          inputs.plasma-manager.homeManagerModules.plasma-manager
          ./home.nix
          ./plasma.nix
          # { home.packages = with inputs.nix-citizen.packages.${pkgs.system}; [ star-citizen ]; }
        ];
      };
    };
}
