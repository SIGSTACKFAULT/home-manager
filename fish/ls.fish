function ll
    ls -l $argv
end

function la
    ls -a $argv
end

function lla
    ll -a $argv
end

if command -q eza
    set -x EZA_COLORS "da=1;35:gm=1;36"
    function ls
        eza --sort=type --icons -F $argv
    end
    function ll
        ls -lb --icons --git $argv
    end
    function lt
        ls --tree --icons --git-ignore $argv
    end
else
    __warn_missing eza
end
