
fish_vi_key_bindings


set -a PATH $HOME/.local/bin/
set -a PATH $HOME/.cargo/bin/
set -x EDITOR vim
set -x VISUAL vim
set -x VIRTUAL_ENV_DISABLE_PROMPT 1
set fish_greeting # no greeting


function __warn_missing
    set_color brblack
    echo "config.fish: missing" \`$argv[1]\`
    set_color normal
end

if status is-interactive
    # Commands to run in interactive sessions can go here
    source (status dirname)/abbreviations.fish
    source (status dirname)/ls.fish
    if command -q zoxide
        zoxide init fish --cmd cd | source
        zoxide init fish --cmd z | source
        if not command -q fzf
            __warn_missing fzf
        end
    else
        __warn_missing zoxide
    end


    if command -q starship
        starship init fish | source
    else
        __warn_missing starship
    end

    if command -q thefuck
        thefuck --alias | source
    else
        __warn_missing thefuck
    end

    if command -q direnv
        direnv hook fish | source
    else
        __warn_missing direnv
    end

    function reload
        if set -q XDG_CONFIG_HOME
            set temp $XDG_CONFIG_HOME/fish/config.fish
        else
            set temp $HOME/.config/fish/config.fish
        end
        set_color brblack
        echo source $temp
        set_color normal
        source $temp
    end

    function mkcd
        mkdir -p $argv[1]
        cd $argv[1]
    end

    function pywhich
        python -c "import $argv[1]; print($argv[1].__file__)"
    end

    function protontricks
        flatpak run com.github.Matoking.protontricks $argv
    end

    function checkout_mr
        if [ $argv[2] ]
            set branch_name $argv[2]
        else
            set branch_name PR-$argv[1]
        end
        git fetch origin refs/pull/$argv[1]/head:$branch_name
        git switch $branch_name
    end

    test -e {$HOME}/.iterm2_shell_integration.fish; and source {$HOME}/.iterm2_shell_integration.fish
end
