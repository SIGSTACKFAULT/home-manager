abbr -a ga -- git add
abbr -a gc -- git commit
abbr -a gca -- git commit --amend
abbr -a gd -- git diff
abbr -a glog -- git log --graph --oneline
abbr -a gstat -- git status
abbr -a gpush -- git push
abbr -a gpsuh -- sl
abbr -a gpull -- git pull
abbr -a gs -- git switch
abbr -a cy -- nice npx cypress
abbr -a hm home-manager

set BIG_EDITOR (basename (command -v codium || command -v code || echo "vim"))
abbr -a c -- $BIG_EDITOR
